import { pxToRem, responsiveFontSizes } from '../utils/getFontValue';

// ----------------------------------------------------------------------

declare module '@mui/material/Typography' {
  interface TypographyPropsVariantOverrides {
    buttonLarge: true;
    buttonMedium: true;
    buttonSmall: true;
  }
}

const FONT_PRIMARY = 'Poppins, sans-serif'; // Google Font
// const FONT_SECONDARY = 'CircularStd, sans-serif'; // Local Font

const typography = {
  fontFamily: FONT_PRIMARY,
  fontWeightRegular: 400,
  fontWeightMedium: 600,
  fontWeightBold: 700,
  h1: {
    fontWeight: 600,
    lineHeight: '77px',
    fontSize: pxToRem(89),
    letterSpacing: 2,
  },
  h2: {
    fontWeight: 500,
    lineHeight: '41.6px',
    fontSize: pxToRem(32),
    ...responsiveFontSizes({ sm: 40, md: 44, lg: 32 }), //Todo: demander à rodolphe responsive
  },
  h3: {
    fontWeight: 500,
    lineHeight: '33.8px',
    fontSize: pxToRem(26),
    ...responsiveFontSizes({ sm: 26, md: 30, lg: 26 }), //Todo: demander à rodolphe responsive
  },
  h4: {
    fontWeight: 500,
    lineHeight: '28.6px',
    fontSize: pxToRem(22),
  },
  h5: {
    fontWeight: 500,
    lineHeight: '26px',
    fontSize: pxToRem(20),
  },
  h6: {
    fontWeight: 500,
    lineHeight: '23.4px',
    fontSize: pxToRem(18),
  },
  subtitle1: {
    fontWeight: 600,
    lineHeight: '24px',
    fontSize: pxToRem(16),
  },
  subtitle2: {
    fontWeight: 600,
    lineHeight: '22px',
    fontSize: pxToRem(14),
  },
  body1: {
    fontWeight: 500,
    lineHeight: '24px',
    fontSize: pxToRem(16),
  },
  body2: {
    fontWeight: 500,
    lineHeight: '21px',
    fontSize: pxToRem(14),
  },
  caption: {
    fontWeight: 500,
    lineHeight: '18.9px',
    fontSize: pxToRem(12),
  },
  overline: {
    fontWeight: 500,
    lineHeight: '18px',
    fontSize: pxToRem(12),
    textTransform: 'uppercase',
  },
  button: {
    fontWeight: 600,
    lineHeight: '26px',
    fontSize: '15px',
    textTransform: 'none',
  },
  buttonLarge: {
    fontWeight: 600,
    lineHeight: '26px',
    fontSize: '15px',
    textTransform: 'none',
  },
  buttonMedium: {
    fontWeight: 600,
    lineHeight: '24px',
    fontSize: '14px',
    textTransform: 'none',
  },
  buttonSmall: {
    fontWeight: 600,
    lineHeight: '22px',
    fontSize: '13px',
    textTransform: 'none',
  },
} as const;

export default typography;
