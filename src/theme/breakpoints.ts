// ----------------------------------------------------------------------

const breakpoints = {
  values: {
    xs: 0,
    sm: 600,
    md: 900,
    lg: 1240,
    xl: 1240,
  },
};

export default breakpoints;
