import React from 'react';
// @date-fns
import { formatDistance } from 'date-fns';
import { fr } from 'date-fns/locale';
// @mui
import {
  Avatar,
  Box,
  Card,
  CardContent,
  CardHeader,
  Chip,
  Paper,
  Typography,
  useTheme,
} from '@mui/material';
// components
import { JobboardCardObjectProps } from '../../@types/JobboardCard.type';
// config
import { JOBBOARD } from '../../config';
// other
import defaultIcon from '../../assets/icon_experiences.svg';

// ----------------------------------------------------------------------

type JobboardCardProps = {
  card: JobboardCardObjectProps;
  hide: boolean;
};

const JobboardCard: React.FC<JobboardCardProps> = ({ card, hide }) => {
  const theme = useTheme();
  const postedDate = card.Date
    ? 'Il y a ' + formatDistance(new Date(card.Date), new Date(), { locale: fr })
    : 'Non défini';

  function capitalizeFirstLetter(str: string) {
    // Met la première lettre en majuscule
    return str ? str.charAt(0).toUpperCase() + str.slice(1) : str;
  }

  function isNumber(str: string) {
    // Vérifie si la string est un nombre
    for (const c of str) {
      if (c !== ' ' && c !== '-' && c >= '0' && c <= '9') return true;
    }
    return false;
  }

  function isZero(str: string) {
    // Vérifie si le string est composé uniquement de 0, espace ou tiret
    for (const c of str) {
      if (c !== '0' && c !== ' ' && c !== '-') return false;
    }
    return true;
  }

  const tjmDisplay = () => {
    // Affiche correctement le Tjm
    if (card.TJM) {
      if (isNumber(card.TJM)) {
        if (!isZero(card.TJM)) return ' • ' + card.TJM + ' €';
        else return null;
      } else return ' • ' + card.TJM;
    } else {
      return null;
    }
  };

  const emojis = ['🏡', '⏳', '🌕']; // [0] = télétravail, [1] = durée, [2] = jour/semaine

  return (
    <Box
      sx={{
        position: 'relative',
      }}
    >
      <Card
        sx={{
          pt: 3,
          px: 3,
          pb: 2,
          height: `${JOBBOARD.CARD_HEIGHT}px`,
          display: 'flex',
          flexDirection: 'column',
          // Le curseur si la carte est cliquable ou pas
          cursor: hide ? 'cursor' : 'pointer',
          // Flou sur les cartes cachées
          filter: hide ? 'blur(4px)' : 'none',
          ...(hide && {
            // Empêche de copier le text sur les cartes cachées
            WebkitUserSelect: 'none',
            MozUserSelect: 'none',
            msUserSelect: 'none',
            userSelect: 'none',
            OUserSelect: 'none',
            KhtmlUserSelect: 'none',
          }),
        }}
      >
        <CardHeader
          title={
            <Typography variant="caption" color={theme.palette.text.disabled}>
              {card.Plateforme}
            </Typography>
          }
          avatar={
            <Avatar
              src={card.Logo === '' ? defaultIcon : card.Logo}
              aria-label="platformIcon"
              variant="rounded"
              sx={{ width: '24px', height: '24px' }}
            />
          }
          sx={{ m: 0, p: 0 }}
        />
        <CardContent sx={{ mt: 3, p: 0 }}>
          <Typography variant="body1" color={theme.palette.text.primary} sx={{ mb: 0.5 }}>
            {card.Title}
          </Typography>
          <Box display="flex">
            <Typography variant="body1" color={theme.palette.primary.lighter}>
              {card.Ville ? card.Ville : null} {tjmDisplay()}
            </Typography>
          </Box>
          <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 1, mt: 3 }}>
            {card.labels?.map((item: string, index: number) =>
              item ? (
                <Chip
                  key={index}
                  variant="filled"
                  size="medium"
                  label={`${emojis[index]} ${capitalizeFirstLetter(item)}`}
                />
              ) : null
            )}
          </Box>
        </CardContent>
      </Card>
      <Box
        display={'flex'}
        justifyItems="end"
        sx={{ mt: 'auto', mb: 0, zIndex: 999, filter: 'blur(0px)' }}
      >
        <Typography
          sx={{ position: 'absolute', right: '0px', bottom: '0px', mr: 3, mb: 2 }}
          variant="caption"
          color={theme.palette.text.disabled}
        >
          {postedDate}
        </Typography>
      </Box>
      {/* la div ci-dessous permet de rendre la card cliquable via Webflow */}
      {!hide && (
        <div
          style={{
            position: 'absolute',
            height: '100%',
            width: '100%',
            transform: `translateY(-${JOBBOARD.CARD_HEIGHT}px)`,
            borderRadius: '16px',
            cursor: 'pointer',
          }}
          dangerouslySetInnerHTML={{
            __html: `<div style="position: absolute; height: 100%; width: 100%; border-radius: 16px" onclick="window.open(\'${card.URL}\')"></div>`,
          }}
        />
      )}
    </Box>
  );
};

export default JobboardCard;
