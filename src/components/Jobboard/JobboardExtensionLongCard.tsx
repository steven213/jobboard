import React from 'react';
// @mui
import { Box, Button, Card, CardContent, Typography, useTheme } from '@mui/material';
// config
import { JOBBOARD } from '../../config';
// ----------------------------------------------------------------------

type JobboardExtensionLongCardProps = {
  assets: { [key: string]: string };
};

const JobboardExtensionLongCard: React.FC<JobboardExtensionLongCardProps> = ({ assets }) => {
  const theme = useTheme();

  return (
    <Card
      sx={{
        pt: 8,
        px: 4,
        height: `${JOBBOARD.CARD_HEIGHT}px`,
        background: `radial-gradient(circle at 115% 160%, rgba(117,0,235,1) 0%, rgba(0,0,0,1) 100%);`,
      }}
    >
      <CardContent
        sx={{
          m: 0,
          p: 0,
          px: 8,
          pb: 6,
          display: 'flex',
        }}
      >
        <Box sx={{ flex: '30%' }}>
          <Typography
            variant="subtitle1"
            color={theme.palette.text.primary}
            textAlign="left"
            whiteSpace={'pre-line'}
          >
            {
              'Met à jour ta dispo à jours sur malt, crème de la crème, le hibou...\n...en un clic sur\
            pylote.'
            }
          </Typography>
          <Box sx={{ mt: 3, position: 'relative' }}>
            <Box sx={{ position: 'absolute', right: '-24px', bottom: '-16px', zIndex: 1 }}>
              <img src={assets['lock']} alt="lock" width="41px" height="41px" />
            </Box>
            <Button size="large" variant="contained" color="inherit">
              <Typography variant="buttonMedium" color={theme.palette.primary.dark}>
                Découvrir l'extension
              </Typography>
              <div
                style={{ position: 'absolute', height: '100%', width: '100%' }}
                dangerouslySetInnerHTML={{
                  __html:
                    '<div style="position: absolute; height: 100%; width: 100%" onclick="window.open(\'https://chrome.google.com/webstore/detail/pylote/bjckcfdgnmppdohidoknphenoflgdapd?utm_source=chrome-ntp-icon\')"></div>',
                }}
              />
            </Button>
          </Box>
          <Typography
            variant="body2"
            color={theme.palette.primary.lighter}
            sx={{ mt: 2 }}
            textAlign="center"
          >
            {'Chrome & Firefox'}
          </Typography>
        </Box>
        <Box sx={{ flex: '70%', position: 'relative' }}>
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'flex-end',
              alignItems: 'flex-end',
              width: '100%',
              height: '100%',
            }}
          >
            <img
              src={assets['avatar']}
              alt="avatar"
              width="111px"
              height="111px"
              style={{ borderRadius: '5px' }}
            />
            <img
              src={assets['logo']}
              alt="logo"
              width="50px"
              height="50px"
              style={{
                position: 'absolute',
                borderRadius: '5px',
                transform: 'translate(50%, 25%)',
              }}
            />
          </Box>
          <img
            src={assets['freelance-info']}
            alt="logo"
            width="60px"
            height="60px"
            style={{
              position: 'absolute',
              borderRadius: '5px',
              bottom: '-15%',
              left: '45%',
            }}
          />
          <img
            src={assets['malt']}
            alt="logo"
            width="60px"
            height="60px"
            style={{
              position: 'absolute',
              borderRadius: '5px',
              bottom: '40%',
              left: '50%',
            }}
          />
          <img
            src={assets['creme']}
            alt="logo"
            width="60px"
            height="60px"
            style={{
              position: 'absolute',
              borderRadius: '5px',
              bottom: '80%',
              left: '70%',
            }}
          />
          <img
            src={assets['comet']}
            alt="logo"
            width="60px"
            height="60px"
            style={{
              position: 'absolute',
              bottom: '90%',
              left: '95%',
            }}
          />
        </Box>
      </CardContent>
    </Card>
  );
};

export default JobboardExtensionLongCard;
