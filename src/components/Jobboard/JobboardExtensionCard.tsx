import React from 'react';
// @mui
import { Box, Button, Card, CardContent, Typography, useTheme } from '@mui/material';
// config
import { JOBBOARD } from '../../config';
// ----------------------------------------------------------------------

type JobboardExtensionCardProps = {
  assets: { [key: string]: string };
};

const JobboardExtensionCard: React.FC<JobboardExtensionCardProps> = ({ assets }) => {
  const theme = useTheme();

  return (
    <Card
      sx={{
        pt: 8,
        px: 4,
        height: `${JOBBOARD.CARD_HEIGHT}px`,
        backgroundColor: theme.palette.primary.main,
      }}
    >
      <CardContent
        sx={{
          m: 0,
          p: 0,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Typography variant="subtitle1" color={theme.palette.text.primary} textAlign="center">
          Les missions les plus récentes sont réservées aux utilisateurs de l’extension.
        </Typography>
        <Box sx={{ mt: 3, position: 'relative' }}>
          <Box sx={{ position: 'absolute', right: '-24px', bottom: '-16px', zIndex: 1 }}>
            <img src={assets['lock']} alt="lock" width="41px" height="41px" />
          </Box>
          <Button size="large" variant="contained" color="inherit">
            <Typography variant="buttonMedium" color={theme.palette.primary.dark}>
              Découvrir l'extension
            </Typography>
            <div
              style={{ position: 'absolute', height: '100%', width: '100%' }}
              dangerouslySetInnerHTML={{
                __html:
                  '<div style="position: absolute; height: 100%; width: 100%" onclick="window.open(\'https://chrome.google.com/webstore/detail/pylote/bjckcfdgnmppdohidoknphenoflgdapd?utm_source=chrome-ntp-icon\')"></div>',
              }}
            />
          </Button>
        </Box>
        <Typography variant="body2" color={theme.palette.primary.lighter} sx={{ mt: 2 }}>
          {'Chrome & Firefox'}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default JobboardExtensionCard;
