// react
import React, { useEffect, useState } from 'react';
// @mui
import { Box, Grid, Typography, useTheme } from '@mui/material';
// components
import { JobboardCardObjectProps } from '../../@types/JobboardCard.type';
import JobboardCard from './JobboardCard';
import JobboardExtensionCard from './JobboardExtensionCard';
import JobboardExtensionLongCard from './JobboardExtensionLongCard';

type Props = {
  jobs: JobboardCardObjectProps[];
  assets: { [key: string]: string };
  platformsTotal: number;
};

const Jobboard: React.FC<Props> = ({ jobs, assets, platformsTotal }) => {
  const theme = useTheme();

  return (
    <Box sx={{ display: 'flex', flexFlow: 'row wrap', gap: '15px' }}>
      {jobs?.length === 0 ? (
        <div>No jobs found</div>
      ) : (
        <>
          <Box sx={{ mt: 11 }}>
            <Typography variant="h6" color={theme.palette.text.secondary}>
              Hello développeur,
            </Typography>
            <Typography variant="h6" color={theme.palette.text.primary}>
              On a réunis pour toi les flux de missions freelance de {platformsTotal} plateformes.
            </Typography>
            <Typography variant="body2" color={theme.palette.text.disabled} sx={{ mt: 8 }}>
              {jobs.length} missions
            </Typography>
          </Box>
          <Grid container spacing={2}>
            {jobs?.map((job: JobboardCardObjectProps, index: number) => {
              if (index === 3) {
                return (
                  <React.Fragment key={index}>
                    <Grid key={'extension-card'} item xs={4} sm={4} md={4} lg={4}>
                      <JobboardExtensionCard assets={assets} />
                    </Grid>
                    <Grid key={index} item xs={4} sm={4} md={4} lg={4}>
                      <JobboardCard card={job} hide={false} />
                    </Grid>
                  </React.Fragment>
                );
              } else if (index === 10) {
                return (
                  <React.Fragment key={index}>
                    <Grid key={index} item xs={4} sm={4} md={4} lg={4}>
                      <JobboardCard card={job} hide={false} />
                    </Grid>
                    <Grid key={'extension-long-card'} item xs={12} sm={12} md={12} lg={12}>
                      <JobboardExtensionLongCard assets={assets} />
                    </Grid>
                  </React.Fragment>
                );
              } else {
                return (
                  <Grid key={index} item xs={4} sm={4} md={4} lg={4}>
                    <JobboardCard card={job} hide={false} />
                  </Grid>
                );
              }
            })}
          </Grid>
        </>
      )}
    </Box>
  );
};

export default Jobboard;
