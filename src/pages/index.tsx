// react
import React from 'react';
// components
import Page from '../components/Page';
import Jobboard from '../components/Jobboard/Jobboard';
import { GetStaticProps } from 'next';
import { Container } from '@mui/material';
import { JobboardCardObjectProps } from '../@types/JobboardCard.type';
// axios
import Axios from 'axios';

// ----------------------------------------------------------------------

export default function HomePage({
  jobs,
  assets,
  platformsTotal,
}: {
  jobs: JobboardCardObjectProps[];
  assets: { [key: string]: string };
  platformsTotal: number;
}) {
  return (
    <Page title="Jobboard">
      <Container maxWidth={'md'}>
        <Jobboard jobs={jobs} assets={assets} platformsTotal={platformsTotal} />
      </Container>
    </Page>
  );
}

export const getStaticProps: GetStaticProps = async (context) => {
  // Récupération des jobs
  const resJobs = await Axios.get('https://api-prod.pylote.io/jobs');
  const jobs = await resJobs.data;

  // Récupération des plateformes
  const resPlatforms = await Axios.get(
    'https://api-prod.pylote.io/recruiter/?displayMissions=true'
  );

  // Récupération des assets
  const resAssets = await Axios.get('http://dev-api.pylote.io/jobboard/assets');
  const platforms = await resPlatforms.data;
  const cards: JobboardCardObjectProps[] = [];
  let assets: { [key: string]: string } = {};

  // ----------------------------------------------------------------------
  // Trie les jobs par date de publication

  jobs.forEach((job: any) => {
    job = JSON.parse(JSON.stringify(job));
    const platform = platforms?.find((platform: any) => job.Plateforme === platform.Name);
    const card: JobboardCardObjectProps = {
      Title: job?.Title || '',
      Plateforme: job?.Plateforme || '',
      Ville: job?.Ville || '',
      Date: job?.Date || '',
      TJM: job?.TJM || '',
      labels:
        [job['A distance/Sur place'] || '', job['Durée'] || '', job['Disponibilité'] || ''] || [],
      URL: job?.URL || '',
      Durée_raw: job?.Durée_raw || '',
      Logo: platform?.Logo[0]?.url || '',
    };
    cards.push(card);
  });
  const noDateCards: JobboardCardObjectProps[] = cards.filter((item: JobboardCardObjectProps) => {
    if (!item.Date) return item;
  });
  // Ajoute les cartes avec date
  const DateCards: JobboardCardObjectProps[] = cards.filter((item: JobboardCardObjectProps) => {
    if (item.Date) return item;
  });
  // Met les cartes avec date puis celles sans date
  const SortedCards = DateCards.sort(
    (a: JobboardCardObjectProps, b: JobboardCardObjectProps) =>
      Date.parse(b.Date) - Date.parse(a.Date)
  ).concat(noDateCards);

  // ----------------------------------------------------------------------
  // Trie les assets

  assets = resAssets.data.reduce((acc: any, asset: any) => {
    acc[asset.fields['Name']] = asset.fields['Attachments'][0]['url'];
    return acc;
  }, {});

  //-----------------------------------------------------------------------
  // Récupération du nombre de plateformes

  const platformsTotal: number = platforms.filter(
    (platform: any) => platform.Missions === true
  ).length;

  return {
    props: {
      jobs: SortedCards,
      assets: assets,
      platformsTotal: platformsTotal,
    },
    revalidate: 10,
  };
};
