export type JobboardCardObjectProps = {
  Plateforme?: string | undefined;
  Title?: string | undefined;
  Ville?: string | undefined;
  TJM?: string | undefined;
  labels?: string[] | undefined;
  URL: string;
  Date: string;
  Durée_raw: string;
  Logo?: string | undefined;
};
